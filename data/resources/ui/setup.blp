using Gtk 4.0;
using Adw 1;

template $FlatsyncSetupWindow: Adw.ApplicationWindow {
  width-request: 360;
  height-request: 170;
  default-width: 800;
  default-height: 600;

  Adw.Breakpoint {
    condition ("max-width: 700")

    setters {
      back_button.visible: false;
      next_button.visible: false;
    }
  }

  Adw.ToolbarView {
    [top]
    Adw.HeaderBar {
      show-title: false;
    }

    [bottom]
    Adw.CarouselIndicatorDots setup_carousel_dots {
      margin-bottom: 50;
    }

    content: Adw.ToastOverlay toast_overlay {
      Overlay {
        [overlay]
        Button back_button {
          icon-name: "go-previous-symbolic";
          opacity: 0;
          halign: start;
          valign: center;
          margin-start: 20;

          styles [
            "circular"
          ]
        }

        [overlay]
        Button next_button {
          icon-name: "go-next-symbolic";
          halign: end;
          valign: center;
          margin-end: 20;

          styles [
            "circular",
            "suggested-action"
          ]
        }

        child: Adw.Carousel setup_carousel {
          vexpand: true;
        };
      }

      EventControllerKey event_controller_key {}
    };
  }
}

Adw.StatusPage welcome_page {
  hexpand: true;
  title: "FlatSync";
  description: _("Keep your Flatpak apps synchronized between devices");
  icon-name: "FlatSync-placeholder";
}

Adw.StatusPage setup_page {
  hexpand: true;
  title: _("Welcome!");
  description: _("Create a new FlatSync instance or connect to an existing one");

  child: FlowBox {
    halign: center;
    homogeneous: true;
    column-spacing: 10;
    row-spacing: 10;
    max-children-per-line: 2;

    ToggleButton create_new_toggle {
      styles [
        "card",
        "setup-page-toggle-button"
      ]

      label: _("_This is my first time using FlatSync");
      active: true;
      height-request: 100;
      use-underline: true;
    }

    ToggleButton login_toggle {
      styles [
        "card",
        "setup-page-toggle-button"
      ]

      label: _("I _already have a FlatSync instance");
      group: create_new_toggle;
      height-request: 100;
      use-underline: true;
    }
  };
}

Adw.StatusPage configure_page {
  hexpand: true;
  title: _("Configure");
  description: _("Please review the settings below");

  child: ListBox {
    halign: center;

    styles [
      "boxed-list"
    ]

    Adw.ComboRow {
      title: _("_Provider");
      subtitle: _("Cloud Provider to synchronize against");
      selectable: false;
      use-underline: true;

      model: StringList {
        strings [
          "Github"
        ]
      };
    }

    Adw.ComboRow {
      title: _("_Installations");
      subtitle: _("Whether to synchronize the Flatpak installation for the system,the current user, or both");
      selectable: false;
      use-underline: true;

      model: StringList {
        strings [
          _("Both"),
          _("User"),
          _("System")
        ]
      };
    }
  };
}

Adw.StatusPage github_page {
  hexpand: true;
  title: _("Github App");
  description: _("To start using FlatSync, please install its Github App");

  child: Button install_github_button {
    label: _("_Install");
    use-underline: true;
    halign: center;
    width-request: 175;

    styles [
      "suggested-action",
      "pill"
    ]
  };
}

Adw.StatusPage gist_id_page {
  hexpand: true;
  title: _("Gist ID");
  description: _("To connect to an existing FlatSync instance, insert its Gist ID. It can be retrieved from FlatSync's \"Preferences\" or the Gist's URL");

  child: Box {
    halign: center;
    orientation: vertical;
    spacing: 30;

    ListBox {
      styles [
        "boxed-list"
      ]

      Adw.EntryRow gist_id_entry {
        title: _("_Gist ID");
        use-underline: true;
        width-request: 330;

        Button gist_id_paste_button {
          icon-name: "clipboard-symbolic";

          styles [
            "flat"
          ]

          tooltip-text: _("Paste");
          valign: center;
          focus-on-click: false;
        }
      }
    }

    Stack open_gists_stack {
      hhomogeneous: true;
      transition-type: crossfade;
      halign: center;
      width-request: 175;

      Button open_gists_button {
        label: _("_Open Gists");
        use-underline: true;

        styles [
          "suggested-action",
          "pill"
        ]
      }

      Button open_gists_button_done {
        label: _("_Valid ID");
        use-underline: true;

        styles [
          "success",
          "pill"
        ]
      }
    }
  };
}
