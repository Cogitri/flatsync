using Gtk 4.0;
using Adw 1;

menu primary_menu {
  section {
    item {
      label: _("_Preferences");
      action: "app.preferences";
    }

    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("_About FlatSync");
      action: "app.about";
    }
  }
}

template $FlatsyncApplicationWindow: Adw.ApplicationWindow {
  // Adw.Dialog's minimum width
  width-request: 360;
  height-request: 150;
  default-width: 550;
  default-height: 500;

  Adw.Breakpoint {
    condition ("max-width: 450")

    setters {
      header_bar.title-widget: null;
      switcher_bar.reveal: true;
    }
  }

  content: Adw.ToolbarView {
    [top]
    Adw.HeaderBar header_bar {
      title-widget: Adw.ViewSwitcher view_switcher {
        stack: view_stack;
        policy: wide;
      };

      [end]
      MenuButton appmenu_button {
        icon-name: "open-menu-symbolic";
        menu-model: primary_menu;
      }
    }

    [bottom]
    Adw.ViewSwitcherBar switcher_bar {
      stack: view_stack;
    }

    content: Stack state_stack {
      transition-type: crossfade;

      Adw.ViewStack view_stack {
        Adw.ViewStackPage {
          name: "sync";
          title: _("_Sync");
          use-underline: true;
          icon-name: "sync-symbolic";

          child: Box {
            orientation: vertical;

            Adw.Banner autosync_status {}

            Stack sync_stack {
              // TODO: Fix the Icon Shading with the Light Style
              Adw.StatusPage idle_state {
                Button sync_now_button {
                  styles [
                    "suggested-action",
                    "pill",
                    "text-button"
                  ]

                  halign: center;

                  Stack sync_button_stack {
                    transition-type: crossfade;

                    Label sync_button_label {
                      label: _("_Sync Now");
                      use-underline: true;
                      mnemonic-widget: sync_now_button;
                      ellipsize: end;
                    }

                    Adw.Spinner sync_button_spinner {}
                  }
                }
              }

              Adw.StatusPage sync_state {
                title: _("Syncing");

                paintable: Adw.SpinnerPaintable {
                  widget: sync_state;
                };
              }
            }
          };
        }

        Adw.ViewStackPage {
          name: "ignored";
          title: _("_Ignored");
          use-underline: true;
          icon-name: "no-sync-symbolic";

          child: Box {
            orientation: vertical;

            Stack ignored_apps_stack {
              Adw.StatusPage ignored_status_page {
                title: _("Ignored Apps");
                description: _("Ignored apps will be excluded from synchronization and only managed locally");
                icon-name: "no-sync-symbolic";

                Button add_apps_button {
                  styles [
                    "suggested-action",
                    "pill",
                    "text-button"
                  ]

                  halign: center;
                  label: _("_Add Apps");
                  use-underline: true;
                }
              }

              Adw.PreferencesPage ignored_apps_page {
                Adw.PreferencesGroup {
                  description: _("Ignored apps will be excluded from synchronization and only managed locally");

                  ListBox ignored_apps_listbox {
                    styles [
                      "boxed-list"
                    ]
                  }
                }
              }
            }
          };
        }
      }

      Adw.StatusPage error_page {
        icon-name: "warning-outline-symbolic";

        child: Button error_page_button {
          styles [
            "suggested-action",
            "pill",
            "text-button"
          ]

          halign: center;
          valign: center;
          vexpand: true;
        };
      }
    };
  };
}
