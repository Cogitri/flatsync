use glib::Boxed;
use std::sync::Arc;
use tokio::runtime::Runtime;

#[derive(Boxed, Clone, Debug)]
#[boxed_type(name = "TokioRuntimeBoxed")]
pub struct TokioRuntimeBoxed(pub Arc<Runtime>);
