use serde::{Deserialize, Serialize};
use zbus::zvariant::{OwnedValue, Type, Value};

use gettextrs::gettext;

#[derive(Deserialize, Serialize, Type, PartialEq, Debug, Value, OwnedValue, Clone, Copy)]
pub enum DaemonError {
    InvalidToken = 0,
}

#[derive(Deserialize, Serialize, Type, PartialEq, Debug, Value, OwnedValue, Clone, Copy)]
pub enum SyncType {
    Automatic = 0,
    Manual = 1,
}

impl std::fmt::Display for DaemonError {
    fn fmt(&self, buffer: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            DaemonError::InvalidToken => write!(buffer, "{}", gettext("Invalid Token")),
        }
    }
}

#[derive(Deserialize, Serialize, Type, PartialEq, Debug, Value, OwnedValue, Clone, Copy)]
pub enum DaemonWarning {
    SyncTimedOut = 0,
}

impl std::fmt::Display for DaemonWarning {
    fn fmt(&self, buffer: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            DaemonWarning::SyncTimedOut => {
                write!(
                    buffer,
                    "{}",
                    gettext("Synchronization Failed, connection timed out")
                )
            }
        }
    }
}

#[derive(Default, Deserialize, Serialize, PartialEq, Debug, Clone, Copy, Type)]
#[zvariant(signature = "s")]
pub enum DaemonState {
    #[default]
    Idle,
    Syncing(SyncType),
    Error(DaemonError),
}

impl std::fmt::Display for DaemonState {
    fn fmt(&self, buffer: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            DaemonState::Idle => write!(buffer, "{}", gettext("Idle")),
            DaemonState::Syncing(sync_type) => match sync_type {
                SyncType::Manual => write!(buffer, "{}", gettext("Syncing Manually")),
                SyncType::Automatic => write!(buffer, "{}", gettext("Syncing")),
            },
            DaemonState::Error(err) => write!(buffer, "{} {}", gettext("Error:"), err),
        }
    }
}

impl From<DaemonState> for Value<'_> {
    fn from(state: DaemonState) -> Self {
        Value::new(serde_json::to_string(&state).unwrap())
    }
}

impl From<Value<'_>> for DaemonState {
    fn from(value: Value<'_>) -> Self {
        let state: String = String::try_from(&value).unwrap();
        serde_json::from_str(&state).unwrap()
    }
}

impl From<OwnedValue> for DaemonState {
    fn from(value: OwnedValue) -> Self {
        let value: Value = value.try_clone().unwrap().into();
        value.into()
    }
}
