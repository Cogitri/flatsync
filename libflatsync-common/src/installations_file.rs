use std::path::PathBuf;

pub fn get_local_installations_file_path() -> PathBuf {
    let mut flatsync_user_data_file = get_user_flatsync_dir_path();
    flatsync_user_data_file.push("flatsync.json");

    flatsync_user_data_file
}

pub fn get_user_flatsync_dir_path() -> PathBuf {
    let mut flatsync_user_data_dir = glib::user_data_dir();
    flatsync_user_data_dir.push("flatsync");

    flatsync_user_data_dir
}
