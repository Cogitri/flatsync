use crate::application::FlatSyncWindow;
use crate::authentication_page::AuthenticationPage;
use crate::setup_completed_page::SetupCompletedPage;
use crate::FlatsyncApplication;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use gtk::gdk;
use gtk::gdk::Display;
use gtk::{
    gio,
    glib::{self, clone, Properties},
};
use libflatsync_common::config::PROFILE;
use libflatsync_common::dbus::DaemonProxy;
use libflatsync_common::providers::github::GitHubProvider;
use libflatsync_common::providers::oauth_client::OauthClientDeviceFlow;
use libflatsync_common::proxy::DaemonProxyBoxed;
use libflatsync_common::runtime::TokioRuntimeBoxed;
use std::cell::Cell;
use std::sync::Arc;
use tokio::runtime::Runtime;

#[derive(Debug, Clone, Copy)]
pub enum SetupOptions {
    CreateNew,
    Login,
}

#[derive(Debug, Clone, Copy)]
enum Direction {
    Forward,
    Backward,
}

#[derive(Debug, Clone)]
pub enum GithubLoginMessage {
    UserCode(String),
    Secret(String),
}

mod imp {
    use super::*;
    use std::cell::{OnceCell, RefCell};

    #[derive(Debug, Properties, gtk::CompositeTemplate)]
    #[properties(wrapper_type = super::FlatsyncSetupWindow)]
    #[template(resource = "/app/drey/FlatSync/ui/setup.ui")]
    pub struct FlatsyncSetupWindow {
        #[template_child]
        pub toast_overlay: TemplateChild<adw::ToastOverlay>,
        #[template_child]
        pub back_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub next_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub setup_carousel: TemplateChild<adw::Carousel>,
        pub carousel_idx: Cell<u32>,
        pub carousel_widgets: RefCell<Vec<gtk::Widget>>,
        #[template_child]
        pub setup_carousel_dots: TemplateChild<adw::CarouselIndicatorDots>,
        #[template_child]
        pub welcome_page: TemplateChild<adw::StatusPage>,
        pub setup_options: Cell<SetupOptions>,
        #[template_child]
        pub setup_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub create_new_toggle: TemplateChild<gtk::ToggleButton>,
        #[template_child]
        pub login_toggle: TemplateChild<gtk::ToggleButton>,
        #[template_child]
        pub configure_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub github_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub install_github_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub gist_id_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub gist_id_entry: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub gist_id_paste_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub open_gists_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub open_gists_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub open_gists_button_done: TemplateChild<gtk::Button>,
        pub authentication_page: OnceCell<RefCell<AuthenticationPage>>,
        pub setup_completed_page: SetupCompletedPage,
        #[template_child]
        pub event_controller_key: TemplateChild<gtk::EventControllerKey>,
        pub gist_id: RefCell<Option<String>>,
        #[property(construct_only, name = "runtime")]
        pub tokio_runtime: OnceCell<TokioRuntimeBoxed>,
        #[property(construct_only, name = "proxy")]
        pub proxy: OnceCell<DaemonProxyBoxed>,
    }

    impl Default for FlatsyncSetupWindow {
        fn default() -> Self {
            Self {
                toast_overlay: TemplateChild::default(),
                back_button: TemplateChild::default(),
                next_button: TemplateChild::default(),
                setup_carousel: TemplateChild::default(),
                carousel_idx: Cell::new(0),
                carousel_widgets: RefCell::new(vec![]),
                setup_carousel_dots: TemplateChild::default(),
                welcome_page: TemplateChild::default(),
                setup_options: Cell::new(SetupOptions::CreateNew),
                setup_page: TemplateChild::default(),
                create_new_toggle: TemplateChild::default(),
                login_toggle: TemplateChild::default(),
                configure_page: TemplateChild::default(),
                github_page: TemplateChild::default(),
                install_github_button: TemplateChild::default(),
                gist_id_page: TemplateChild::default(),
                gist_id_entry: TemplateChild::default(),
                gist_id_paste_button: TemplateChild::default(),
                open_gists_stack: TemplateChild::default(),
                open_gists_button: TemplateChild::default(),
                open_gists_button_done: TemplateChild::default(),
                authentication_page: OnceCell::new(),
                setup_completed_page: SetupCompletedPage::new(false),
                event_controller_key: TemplateChild::default(),
                gist_id: RefCell::new(None),
                tokio_runtime: OnceCell::new(),
                proxy: OnceCell::new(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FlatsyncSetupWindow {
        const NAME: &'static str = "FlatsyncSetupWindow";
        type Type = super::FlatsyncSetupWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for FlatsyncSetupWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            obj.imp()
                .authentication_page
                .set(RefCell::new(AuthenticationPage::new(
                    false,
                    &obj.imp().toast_overlay,
                )))
                .unwrap();

            obj.setup_github_login();
            obj.setup_welcome_page();
            obj.init_carousel();
            obj.connect_handlers();
        }
    }

    impl WidgetImpl for FlatsyncSetupWindow {}
    impl WindowImpl for FlatsyncSetupWindow {}
    impl ApplicationWindowImpl for FlatsyncSetupWindow {}
    impl AdwWindowImpl for FlatsyncSetupWindow {}
    impl AdwApplicationWindowImpl for FlatsyncSetupWindow {}
}

glib::wrapper! {
    pub struct FlatsyncSetupWindow(ObjectSubclass<imp::FlatsyncSetupWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::Window,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl FlatsyncSetupWindow {
    pub fn new(
        app: &FlatsyncApplication,
        tokio_runtime: Arc<Runtime>,
        proxy: DaemonProxy<'static>,
    ) -> Self {
        glib::Object::builder()
            .property("application", app)
            .property("runtime", TokioRuntimeBoxed(tokio_runtime))
            .property("proxy", DaemonProxyBoxed(proxy))
            .build()
    }

    fn proxy(&self) -> &DaemonProxy<'static> {
        &self.imp().proxy.get().unwrap().0
    }

    // This selects the icon in the Welcome Page and resizes/positions it
    fn setup_welcome_page(&self) {
        let imp = self.imp();

        let welcome_page = imp.welcome_page.get();
        let icon: gtk::Image = welcome_page
            .first_child()
            .unwrap()
            .first_child()
            .unwrap()
            .first_child()
            .unwrap()
            .first_child()
            .unwrap()
            .first_child()
            .unwrap()
            .first_child()
            .unwrap()
            .downcast()
            .unwrap();

        //Maximum usable size
        icon.set_pixel_size(336);
        icon.add_css_class("setup_page_icon")
    }

    pub fn init_carousel(&self) {
        let imp = self.imp();

        imp.setup_carousel_dots
            .set_carousel(Some(&imp.setup_carousel.get()));

        self.carousel_append_widget(imp.welcome_page.get().into());
        self.carousel_append_widget(imp.setup_page.get().into());
        self.carousel_append_widget(imp.configure_page.get().into());
        self.carousel_append_widget(imp.github_page.get().into());
    }

    fn carousel_append_widget(&self, widget_to_add: gtk::Widget) -> Option<()> {
        let imp = self.imp();
        if imp.carousel_widgets.borrow().contains(&widget_to_add) {
            None
        } else {
            imp.setup_carousel.append(&widget_to_add);
            imp.carousel_widgets.borrow_mut().push(widget_to_add);

            Some(())
        }
    }

    fn carousel_insert_widget(&self, widget_to_add: gtk::Widget, idx: usize) -> Option<()> {
        let imp = self.imp();

        if imp.carousel_widgets.borrow().contains(&widget_to_add) {
            None
        } else {
            imp.setup_carousel
                .insert(&widget_to_add, idx.try_into().unwrap());
            imp.carousel_widgets.borrow_mut().insert(idx, widget_to_add);

            Some(())
        }
    }

    fn carousel_remove_widget(&self, widget_to_remove: gtk::Widget) -> Option<()> {
        let imp = self.imp();
        let idx = imp
            .carousel_widgets
            .borrow()
            .iter()
            .position(|widget| widget == &widget_to_remove)?;

        imp.setup_carousel.remove(&widget_to_remove);
        imp.carousel_widgets.borrow_mut().remove(idx);

        Some(())
    }

    fn carousel_substitute_widget(
        &self,
        widget_to_remove: gtk::Widget,
        widget_to_add: gtk::Widget,
    ) -> Option<()> {
        let imp = self.imp();
        let idx = imp
            .carousel_widgets
            .borrow()
            .iter()
            .position(|widget| widget == &widget_to_remove)?;

        self.carousel_remove_widget(widget_to_remove);
        self.carousel_insert_widget(widget_to_add, idx);

        Some(())
    }

    fn setup_github_login(&self) {
        let imp = self.imp();
        let github = GitHubProvider::new();

        let runtime = imp.tokio_runtime.get().unwrap();
        let (sender, receiver) = async_channel::bounded(2);

        runtime.0.spawn(clone!(
            #[strong]
            sender,
            async move {
                loop {
                    if let Ok(device_auth_res) = github.device_code().await {
                        sender
                            .send(GithubLoginMessage::UserCode(
                                device_auth_res.user_code().secret().to_string(),
                            ))
                            .await
                            .unwrap();

                        if let Ok(token_pair) = github.register_device(device_auth_res).await {
                            sender
                                .send(GithubLoginMessage::Secret(
                                    serde_json::to_string(&token_pair).unwrap(),
                                ))
                                .await
                                .unwrap();
                            break;
                        }
                    }
                }
            }
        ));

        glib::spawn_future_local(clone!(
            #[weak(rename_to = obj)]
            self,
            async move {
                while let Ok(msg) = receiver.recv().await {
                    match msg {
                        GithubLoginMessage::UserCode(code) => obj
                            .imp()
                            .authentication_page
                            .get()
                            .unwrap()
                            .borrow_mut()
                            .set_token_row_title(&code),
                        GithubLoginMessage::Secret(secret) => obj.complete_login(&secret).await,
                    }
                }
            }
        ));
    }

    async fn complete_login(&self, secret: &str) {
        let imp = self.imp();
        let proxy = self.proxy();

        proxy.set_gist_secret(secret).await.unwrap();

        let gist_id = &imp.gist_id.borrow().clone();

        if let Some(id) = gist_id {
            proxy.set_gist_id(id.as_ref()).await.unwrap();
        } else {
            proxy.create_gist().await.unwrap();
        }

        self.carousel_append_widget(imp.setup_completed_page.clone().into());
        imp.authentication_page
            .get()
            .unwrap()
            .borrow()
            .complete_login();
    }

    async fn validate_gist_id(&self, gist_id: String) {
        let imp = self.imp();
        let runtime = imp.tokio_runtime.get().unwrap();
        let (sender, receiver) = async_channel::bounded(1);
        let proxy = self.proxy();

        runtime.0.spawn(clone!(
            #[strong]
            proxy,
            #[strong]
            gist_id,
            async move {
                match proxy.gist_exists(gist_id).await.unwrap() {
                    true => sender.send(true).await,
                    false => sender.send(false).await,
                }
            }
        ));

        glib::spawn_future_local(clone!(
            #[weak(rename_to = obj)]
            self,
            async move {
                while let Ok(msg) = receiver.recv().await {
                    match msg {
                        true => {
                            obj.imp()
                                .open_gists_stack
                                .set_visible_child(&obj.imp().open_gists_button_done.get());
                            obj.carousel_append_widget(
                                obj.imp()
                                    .authentication_page
                                    .get()
                                    .unwrap()
                                    .borrow_mut()
                                    .clone()
                                    .into(),
                            );
                            obj.imp().create_new_toggle.set_sensitive(false);
                            obj.imp().gist_id.replace(Some(gist_id.clone()));
                        }
                        false => {
                            obj.imp()
                                .open_gists_stack
                                .set_visible_child(&obj.imp().open_gists_button.get());
                            obj.carousel_remove_widget(
                                obj.imp()
                                    .authentication_page
                                    .get()
                                    .unwrap()
                                    .borrow_mut()
                                    .clone()
                                    .into(),
                            );
                        }
                    }
                }
            }
        ));
    }

    fn navigate_carousel(&self, direction: Direction) {
        let imp = self.imp();

        let idx: usize = match direction {
            Direction::Forward => (imp.carousel_idx.get() + 1).try_into().unwrap(),
            Direction::Backward => match imp.carousel_idx.get() {
                0 => 0,
                _ => (imp.carousel_idx.get() - 1).try_into().unwrap(),
            },
        };
        let binding = imp.carousel_widgets.borrow();
        let widget = binding.get(idx);

        if let Some(widget) = widget {
            imp.setup_carousel.scroll_to(widget, true);
        }
    }

    fn update_navigation_controls(&self) {
        let imp = self.imp();

        let position = imp.setup_carousel.position();
        let penultimate_page = imp.setup_carousel.n_pages() as f64 - 2.0;

        let next_button_opacity = if position <= penultimate_page {
            1.0
        } else {
            1.0 - (position - penultimate_page)
        };

        imp.back_button.set_opacity(position);
        imp.next_button.set_opacity(next_button_opacity);
    }

    fn connect_handlers(&self) {
        let imp = self.imp();
        let obj = self.clone();
        let obj2 = self.clone();
        let obj3 = self.clone();

        imp.back_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.navigate_carousel(Direction::Backward);
            }
        ));

        imp.next_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.navigate_carousel(Direction::Forward);
            }
        ));
        imp.open_gists_button_done.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.navigate_carousel(Direction::Forward);
            }
        ));

        imp.authentication_page
            .get()
            .unwrap()
            .borrow()
            .connect_local("login-done", false, move |_| {
                obj.navigate_carousel(Direction::Forward);
                None
            });

        imp.setup_carousel.connect_page_changed(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_, idx| {
                if obj.imp().carousel_idx.get() < idx {
                    obj.imp().next_button.grab_focus();
                } else {
                    obj.imp().back_button.grab_focus();
                }
                obj.imp().carousel_idx.set(idx);
            }
        ));
        imp.setup_carousel.connect_position_notify(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.update_navigation_controls();
            }
        ));
        imp.setup_carousel.connect_n_pages_notify(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                // TODO: Smoothly animate navigation buttons' appearance
                obj.update_navigation_controls();
            }
        ));

        imp.create_new_toggle.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.imp().setup_options.set(SetupOptions::CreateNew);
                obj.carousel_substitute_widget(
                    obj.imp().gist_id_page.get().into(),
                    obj.imp().github_page.get().into(),
                );
            }
        ));
        imp.login_toggle.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                obj.imp().setup_options.set(SetupOptions::Login);
                obj.carousel_substitute_widget(
                    obj.imp().github_page.get().into(),
                    obj.imp().gist_id_page.get().into(),
                );
            }
        ));

        imp.install_github_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                let uri_launcher =
                    gtk::UriLauncher::new("https://github.com/apps/flatsync/installations/new");
                uri_launcher.launch(
                    Some(&obj),
                    Some(&gio::Cancellable::new()),
                    clone!(
                        #[weak]
                        obj,
                        move |result| {
                            if result.is_ok() {
                                obj.imp()
                                    .install_github_button
                                    .remove_css_class("suggested-action");
                                obj.carousel_append_widget(
                                    obj.imp()
                                        .authentication_page
                                        .get()
                                        .unwrap()
                                        .borrow_mut()
                                        .clone()
                                        .into(),
                                );
                                obj.imp().login_toggle.set_sensitive(false);
                            }
                        }
                    ),
                );
            }
        ));

        imp.gist_id_entry.connect_changed(clone!(
            #[weak(rename_to = obj)]
            self,
            move |entry| {
                if obj.imp().open_gists_stack.visible_child().unwrap()
                    == obj.imp().open_gists_button_done.get()
                {
                    obj.imp()
                        .open_gists_stack
                        .set_visible_child(&obj.imp().open_gists_button.get());
                    obj.carousel_remove_widget(
                        obj.imp()
                            .authentication_page
                            .get()
                            .unwrap()
                            .borrow_mut()
                            .clone()
                            .into(),
                    );
                }

                let text = entry.text().trim().to_lowercase();

                if text.len() == 32 {
                    glib::spawn_future_local(async move {
                        obj.validate_gist_id(text).await;
                    });
                }
            }
        ));
        imp.gist_id_paste_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                Display::default().unwrap().clipboard().read_text_async(
                    Some(&gio::Cancellable::new()),
                    move |result| {
                        if let Ok(Some(text)) = result {
                            obj.imp().gist_id_entry.set_text(text.trim());
                            obj.imp()
                                .toast_overlay
                                .add_toast(adw::Toast::new(&gettext("Pasted from Clipboard")));
                        }
                    },
                );
            }
        ));
        imp.open_gists_button.connect_clicked(clone!(
            #[weak(rename_to = obj)]
            self,
            move |_| {
                let uri_launcher = gtk::UriLauncher::new("https://gist.github.com/mine");
                uri_launcher.launch(Some(&obj), Some(&gio::Cancellable::new()), |_| {});
            }
        ));

        imp.authentication_page
            .get()
            .unwrap()
            .borrow()
            .connect_local("open-uri", false, move |links| {
                let uri_launcher = gtk::UriLauncher::new(links[1].get().unwrap());
                uri_launcher.launch(Some(&obj2), Some(&gio::Cancellable::new()), |_| {});
                None
            });

        imp.setup_completed_page
            .connect_local("continue-button-pressed", false, move |_| {
                let application: FlatsyncApplication =
                    obj3.application().unwrap().downcast().unwrap();
                application.switch_window(FlatSyncWindow::MainWindow);
                None
            });

        imp.event_controller_key.connect_key_pressed(clone!(
            #[strong(rename_to = obj)]
            self,
            move |_, keyval, _, _| {
                if keyval == gdk::Key::Right {
                    obj.navigate_carousel(Direction::Forward);
                } else if keyval == gdk::Key::Left {
                    obj.navigate_carousel(Direction::Backward);
                }
                glib::Propagation::Proceed
            }
        ));
    }
}
