use crate::glib::{clone, WeakRef};
use crate::preferences::FlatsyncPreferencesDialog;
use crate::setup::FlatsyncSetupWindow;
use crate::window::FlatsyncApplicationWindow;
use adw::prelude::AdwDialogExt;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use gtk::{
    prelude::*,
    {gdk, gio, glib},
};
use libflatsync_common::config::{APP_ID, PKGDATADIR, PROFILE, VERSION};
use libflatsync_common::dbus::DaemonProxy;
use log::{debug, info};
use std::sync::Arc;
use tokio::runtime::Runtime;

mod imp {
    use super::*;
    use glib::WeakRef;
    use once_cell::sync::OnceCell;

    #[derive(Debug, Default)]
    pub struct FlatsyncApplication {
        pub window: OnceCell<WeakRef<FlatsyncApplicationWindow>>,
        pub setup_window: OnceCell<WeakRef<FlatsyncSetupWindow>>,
        pub tokio_runtime: OnceCell<Arc<Runtime>>,
        pub proxy: OnceCell<DaemonProxy<'static>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FlatsyncApplication {
        const NAME: &'static str = "FlatsyncApplication";
        type Type = super::FlatsyncApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for FlatsyncApplication {}

    impl ApplicationImpl for FlatsyncApplication {
        fn activate(&self) {
            debug!("GtkApplication<FlatsyncApplication>::activate");
            self.parent_activate();
            let app = self.obj();
            glib::MainContext::default().block_on(clone!(
                #[weak]
                app,
                async move {
                    if app.proxy().is_initialised().await.unwrap() {
                        app.switch_window(FlatSyncWindow::MainWindow);
                    } else {
                        app.switch_window(FlatSyncWindow::SetupWindow);
                    }
                }
            ));
        }

        fn startup(&self) {
            debug!("GtkApplication<FlatsyncApplication>::startup");
            self.parent_startup();
            let app = self.obj();

            // Set icons for shell
            gtk::Window::set_default_icon_name(APP_ID);

            app.setup_css();
            app.setup_gactions();
            app.setup_accels();

            self.tokio_runtime
                .set(Arc::new(Runtime::new().unwrap()))
                .unwrap();

            // Init zbus proxy
            let (sender, mut reciever) = tokio::sync::mpsc::channel::<DaemonProxy>(1);

            self.tokio_runtime.get().unwrap().spawn(async move {
                let connection = zbus::Connection::session().await.unwrap();
                let proxy = libflatsync_common::dbus::DaemonProxy::new(&connection)
                    .await
                    .unwrap();

                sender.send(proxy).await.unwrap();
            });

            self.proxy.set(reciever.blocking_recv().unwrap()).unwrap();
        }
    }

    impl GtkApplicationImpl for FlatsyncApplication {}
    impl AdwApplicationImpl for FlatsyncApplication {}
}

glib::wrapper! {
    pub struct FlatsyncApplication(ObjectSubclass<imp::FlatsyncApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

pub enum FlatSyncWindow {
    MainWindow,
    SetupWindow,
}

impl FlatsyncApplication {
    fn main_window(&self) -> Option<FlatsyncApplicationWindow> {
        Some(self.imp().window.get()?.upgrade().unwrap())
    }

    fn setup_window(&self) -> Option<FlatsyncSetupWindow> {
        Some(self.imp().setup_window.get()?.upgrade().unwrap())
    }

    fn proxy(&self) -> &DaemonProxy<'static> {
        self.imp().proxy.get().unwrap()
    }

    pub fn switch_window(&self, window: FlatSyncWindow) {
        let imp = self.imp();
        let tokio_runtime = imp.tokio_runtime.get().unwrap().clone();
        let proxy = imp.proxy.get().unwrap().clone();

        match window {
            FlatSyncWindow::MainWindow => {
                if self.try_present_window(imp.window.get()) {
                    return;
                };

                let window = FlatsyncApplicationWindow::new(self, tokio_runtime, proxy);
                imp.window
                    .set(window.downgrade())
                    .expect("Window already set.");

                self.try_close_window(self.setup_window());
                self.main_window().unwrap().present();
            }
            FlatSyncWindow::SetupWindow => {
                if self.try_present_window(imp.setup_window.get()) {
                    return;
                };

                let window = FlatsyncSetupWindow::new(self, tokio_runtime, proxy);
                imp.setup_window
                    .set(window.downgrade())
                    .expect("Window already set.");

                self.try_close_window(self.main_window());
                self.setup_window().unwrap().present();
            }
        }
    }

    fn try_present_window<Window>(&self, window_to_open: Option<&WeakRef<Window>>) -> bool
    where
        Window: IsA<gtk::Window>,
    {
        if let Some(window) = window_to_open {
            let window = window.upgrade().unwrap();
            window.present();
            return true;
        }
        false
    }

    fn try_close_window<Window>(&self, window_to_close: Option<Window>)
    where
        Window: IsA<gtk::Window>,
    {
        if let Some(window) = window_to_close {
            window.close();
        }
    }

    fn setup_gactions(&self) {
        // Quit
        let action_quit = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| {
                // This is needed to trigger the delete event and saving the window state
                app.main_window().unwrap().close();
                app.quit();
            })
            .build();

        // About
        let action_about = gio::ActionEntry::builder("about")
            .activate(|app: &Self, _, _| {
                app.show_about_dialog();
            })
            .build();
        // Preferences
        let action_preferences = gio::ActionEntry::builder("preferences")
            .activate(|app: &Self, _, _| {
                app.show_preferences_dialog();
            })
            .build();
        self.add_action_entries([action_quit, action_about, action_preferences]);
    }

    // Sets up keyboard shortcuts
    fn setup_accels(&self) {
        self.set_accels_for_action("app.quit", &["<Control>q"]);
        self.set_accels_for_action("window.close", &["<Control>w"]);
        self.set_accels_for_action("app.preferences", &["<Control>comma"]);
    }

    fn setup_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/app/drey/FlatSync/style.css");
        if let Some(display) = gdk::Display::default() {
            gtk::style_context_add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }

    fn show_about_dialog(&self) {
        let about_dialog = adw::AboutDialog::builder()
            .application_icon(APP_ID)
            .application_name("Flatsync")
            .developer_name("Rasmus Thomsen")
            .version(VERSION)
            // TODO: Add License
            .translator_credits(gettext("translator-credits"))
            .developers(vec!["Rasmus Thomsen"])
            .artists(vec!["Rasmus Thomsen"])
            .website("https://gitlab.gnome.org/Cogitri/flatsync")
            .issue_url("https://gitlab.gnome.org/Cogitri/flatsync/-/issues")
            .build();

        about_dialog.present(Some(&self.main_window().unwrap()));
    }

    fn show_preferences_dialog(&self) {
        let main_window = &self.main_window().unwrap();

        let preferences_dialog =
            FlatsyncPreferencesDialog::new(self.imp().proxy.get().unwrap().clone());

        preferences_dialog.present(Some(main_window));
    }

    pub fn run(&self) -> glib::ExitCode {
        info!("FlatSync ({})", APP_ID);
        info!("Version: {} ({})", VERSION, PROFILE);
        info!("Datadir: {}", PKGDATADIR);

        ApplicationExtManual::run(self)
    }
}

impl Default for FlatsyncApplication {
    fn default() -> Self {
        glib::Object::builder()
            .property("application-id", APP_ID)
            .property("resource-base-path", "/app/drey/FlatSync/")
            .build()
    }
}
