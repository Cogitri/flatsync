use ashpd::desktop::notification::Priority;
use ashpd::desktop::Icon;
use gettextrs::gettext;
use libflatsync_common::config::APP_ID;
use libflatsync_common::state::{DaemonError, DaemonWarning};

pub enum ToNotify {
    ManualSyncCompleted,
    Warning(DaemonWarning),
    Error(DaemonError),
}

pub struct NotificationContent {
    pub title: String,
    pub subtitle: String,
    pub priority: Priority,
    // To load the Icon with ASHPD it needs to be exported; See data/icons/meson.build
    pub icon: Option<Icon>,
}

impl ToNotify {
    pub fn notification_content(&self) -> NotificationContent {
        match self {
            ToNotify::ManualSyncCompleted => NotificationContent {
                title: gettext("Synchronization Complete"),
                subtitle: gettext("Flatpak apps are now synced"),
                priority: Priority::Normal,
                icon: None,
            },
            ToNotify::Warning(warning) => match warning {
                DaemonWarning::SyncTimedOut => NotificationContent {
                    title: gettext("Synchronization Failed"),
                    subtitle: gettext("Connection timed out, is the system online?"),
                    priority: Priority::Normal,
                    icon: load_icon("warning-outline-symbolic"),
                },
            },
            ToNotify::Error(error) => match error {
                DaemonError::InvalidToken => NotificationContent {
                    title: gettext("Login Expired"),
                    subtitle: gettext("Open the app to reauthenticate"),
                    priority: Priority::High,
                    icon: load_icon("warning-outline-symbolic"),
                },
            },
        }
    }
}

fn load_icon(name: &str) -> Option<Icon> {
    Some(Icon::with_names([format!("{}.{}", APP_ID, name)]))
}
