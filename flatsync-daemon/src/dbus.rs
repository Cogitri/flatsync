use crate::context::Context;
use crate::notify::ToNotify;
use crate::{imp::Impl, DBusError};
use gettextrs::gettext;
use libflatsync_common::state::{DaemonState, SyncType};
use libflatsync_common::FlatpakInstallationPayload;
use libflatsync_common::FrontendFlatpakRef;
use log::{debug, info};
use std::sync::Arc;
use tokio::sync::RwLock;
use zbus::{interface, SignalContext};

use ashpd::desktop::inhibit::InhibitFlags;
use ashpd::desktop::inhibit::InhibitProxy;
use ashpd::desktop::Request;
use ashpd::WindowIdentifier;

use crate::MessageType;

pub struct Daemon {
    state: DaemonState,
    imp: Impl,
    context: Arc<RwLock<Context>>,
    sender: tokio::sync::mpsc::Sender<MessageType>,
    inhibit_request: Option<Request<()>>,
}

impl Daemon {
    pub async fn new(
        context: Arc<RwLock<Context>>,
        sender: tokio::sync::mpsc::Sender<MessageType>,
    ) -> Result<Self, crate::Error> {
        let imp = Impl::new().await;

        Ok(Self {
            state: DaemonState::default(),
            context,
            imp,
            sender,
            inhibit_request: None,
        })
    }
}

#[interface(name = "app.drey.FlatSync.Daemon0")]
impl Daemon {
    #[zbus(property)]
    fn state(&self) -> DaemonState {
        self.state
    }

    #[zbus(property)]
    async fn set_state(&mut self, state: DaemonState) {
        let old_state = self.state;
        self.state = state;

        if let DaemonState::Syncing(_) = state {
            self.inhibit_request = inhibit_session_end().await.into()
        } else if let Some(inhibit_request) = &self.inhibit_request {
            inhibit_request.close().await.unwrap();
            self.inhibit_request = None;
        }

        if let DaemonState::Error(error) = state {
            self.imp.notify(ToNotify::Error(error)).await.unwrap();
        }

        if old_state == DaemonState::Syncing(SyncType::Manual) && state == DaemonState::Idle {
            self.imp
                .notify(ToNotify::ManualSyncCompleted)
                .await
                .unwrap();
        }
    }

    async fn app_list(&self) -> Vec<FrontendFlatpakRef> {
        self.context
            .read()
            .await
            .local_installations()
            .frontend_flatpak_refs()
    }

    async fn ignored_apps(&self) -> Vec<String> {
        self.context
            .read()
            .await
            .local_installations()
            .ignored_apps
            .clone()
    }

    async fn add_ignored_apps(&self, ids: Vec<String>) -> bool {
        let mut payload = FlatpakInstallationPayload::new_from_system().unwrap();
        if !payload.add_ignored_apps(ids) {
            return false;
        }

        self.context
            .write()
            .await
            .set_cache_and_file(payload)
            .unwrap();
        self.sync_now(SyncType::Automatic).await.unwrap();

        true
    }

    async fn remove_ignored_app(&self, id: String) -> bool {
        let mut payload = FlatpakInstallationPayload::new_from_system().unwrap();
        if !payload.remove_ignored_app(&id) {
            return false;
        };

        self.context
            .write()
            .await
            .set_cache_and_file(payload)
            .unwrap();
        self.sync_now(SyncType::Automatic).await.unwrap();

        true
    }

    async fn is_initialised(&self) -> bool {
        self.imp.is_initialised()
    }

    async fn set_gist_secret(
        &mut self,
        #[zbus(signal_context)] ctxt: SignalContext<'_>,
        secret: &str,
    ) -> Result<(), DBusError> {
        if secret.is_empty() {
            return Err(DBusError::InvalidSecret);
        }
        self.imp
            .set_gist_secret(secret)
            .await
            .map_err(|_| DBusError::InvalidSecret)?;

        self.set_state(DaemonState::Idle).await;
        self.state_changed(&ctxt).await.unwrap();

        self.sync_now(SyncType::Automatic).await
    }

    /// ## `CreateGist(...)`
    /// Create a remote gist with the list of local Flatpak installations and get the gist file ID
    async fn create_gist(&mut self) -> Result<String, DBusError> {
        self.imp.create_gist().await.map_err(|e| {
            debug!("Error creating gist: {:?}", e);
            DBusError::GistCreateFailure(e.to_string())
        })
    }

    async fn gist_exists(&self, gist_id: String) -> Result<bool, DBusError> {
        self.imp.gist_exists(gist_id).await.map_err(|e| {
            debug!("Error fetching gist: {:?}", e);
            DBusError::GistFetchFailure(e.to_string())
        })
    }

    /// ## `UpdateGist(..)`
    /// Update the remote gist with the list of local Flatpak installations
    async fn post_gist(&self) -> Result<(), DBusError> {
        self.imp
            .post_gist()
            .await
            .map_err(|e| DBusError::GistUpdateFailure(e.to_string()))?;
        info!("Gist successfully updated");
        Ok(())
    }

    async fn set_gist_id(&self, id: &str) -> Result<(), DBusError> {
        self.imp.set_gist_id(id);
        self.sync_now(SyncType::Automatic).await
    }

    async fn sync_now(&self, sync_type: SyncType) -> Result<(), DBusError> {
        info!("Starting Manual Sync");
        self.sender
            .send(MessageType::TimeToPoll(sync_type))
            .await
            .map_err(|_| DBusError::SendError)
    }

    async fn autosync(&self) -> Result<bool, DBusError> {
        Ok(self.imp.autosync())
    }

    async fn set_autosync(&self, autosync: bool) -> Result<(), DBusError> {
        self.imp.set_autosync(autosync);
        Ok(())
    }

    async fn autosync_timer(&self) -> Result<u32, DBusError> {
        Ok(self.imp.autosync_timer())
    }

    async fn set_autosync_timer(&self, timer: u32) -> Result<(), DBusError> {
        self.imp.set_autosync_timer(timer);
        Ok(())
    }

    async fn autostart_file(&mut self, install: bool) -> Result<(), DBusError> {
        self.imp
            .autostart_file(install)
            .await
            .map_err(|_| DBusError::AutoStartFailure)
    }
}

async fn inhibit_session_end() -> Request<()> {
    let proxy = InhibitProxy::new().await.unwrap();
    let identifier = WindowIdentifier::default();

    proxy
        .inhibit(
            &identifier,
            InhibitFlags::Logout
                | InhibitFlags::UserSwitch
                | InhibitFlags::Suspend
                | InhibitFlags::Idle,
            &gettext("Syncing in progress"),
        )
        .await
        .unwrap()
}
